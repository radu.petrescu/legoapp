import { Component, OnInit } from '@angular/core';
import { Product, ShopService } from './shop.service';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {

  products:Product[];
  currentProduct:Product;
  popupVisible = false;

  constructor(service:ShopService) {
    this.products = service.getProducts();
    this.currentProduct = this.products[0];
  }

  showProduct(product: Product) {
    this.currentProduct = product;
    this.popupVisible = true;
  }

  ngOnInit(): void {
  }

}
