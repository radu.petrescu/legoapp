import { Injectable } from '@angular/core';

export interface Product {
  id : number;
  title : string;
  price : number;
  description : string;
  image : string;
}

const products : Product[] = [
  {
  id:1,
  title:"Fire Dragon",
  price: 200,
  description : "Help Emily Jones and Azari investigate the Fire Dragon's Lava Cave and have a cozy campfire. Featuring the Lava Cave with lava fall function, Shadow Fountain with a function to turn the water from cursed to cleansed, a campfire and other accessories. Includes Zonya the fire dragon, with space on her back for both the Emily Jones and Azari Firedancer mini-doll figures.",
  image: "../../assets/img/set1.png"
  },
  {
    id:2,
    title:"Water Dragon",
    price: 300,
    description : "Incredible stories from the NINJAGO®: Seabound TV series can be played out by kids with this highly detailed Water Dragon (71754) playset. The impressive dragon toy has a posable head, jaw, legs, neck, wings and a moving tail that can be used as a weapon, to help fuel hours of enriching play for youngsters.",
    image: "../../assets/img/set2.png"
  },
  {
    id:3,
    title:"Golden Dragon",
    price: 500,
    description : "Kids aged 9 and up can stage their favorite scenes from the NINJAGO®: Crystallized TV series with Lloyd’s Golden Ultra Dragon (71774). The dragon playset includes NINJAGO’s biggest ever dragon, featuring 4 heads, highly posable legs and tail, and a pair of wings that unfolds into 3 blades – all in gold!",
    image: "../../assets/img/set3.png"
  },
  {
    id:4,
    title:"Green Dragon",
    price: 250,
    description : "Know an Avatar fan or child aged 9 and up who deserves a reward? This LEGO® Avatar Neytiri & Thanator vs. AMP Suit Quaritch (75571) building toy set will hit the mark. The set includes minifigures of Neytiri and Colonel Miles Quaritch, posable models of Neytiri’s Thanator figure and the Colonel’s AMP suit, plus a rainforest build with glow-in-the-dark pieces.",
    image: "../../assets/img/set4.png"
  },
  {
    id:5,
    title:"Lego Titan Mech",
    price: 150,
    description : "Get young ninja fans role-playing exciting scenes from season 5 of the LEGO® NINJAGO® TV series, with this Zane’s Titan Mech Battle (71738) Legacy set. Featuring posable legs and arms, a sword and spinning chainsaw in its hands, plus 2 spring-loaded shooters and a cockpit for ninja minifigures, kids will love this modern update of the original mech.",
    image: "../../assets/img/set5.png"
  },
  {
    id:6,
    title:"Fire Mech",
    price: 500,
    description : "Get set for sizzling battle action with this huge and highly posable THE LEGO® NINJAGO® MOVIE™ mech. The Fire Mech features an opening minifigure cockpit, 2 shoulder disc shooters and 2 arms with non-shooting fire blasters. Includes 6 minifigures with assorted weapons—including Kai’s katanas and Zane’s bow and arrow—to offer even more thrilling role-play possibilities.",
    image: "../../assets/img/set6.png"
  },
  {
    id:7,
    title:"Quake Mech",
    price: 350,
    description : "Fire ‘soundwaves’ from Cole’s Quake Mech to save TV reporter Fred Finley from the shark army, with this all-action THE LEGO® NINJAGO® MOVIE™ 70632 Quake Mech building set. The highly posable, uni-wheel Quake Mech features an opening minifigure cockpit, fold-up shoulder ‘loudspeakers' with 8 hidden ‘soundwave'-shooting flick missiles, fully posable arms with gripping fists and a big wheel with stabilizer. This ninja set also includes 5 minifigures with assorted weapons to intensify the role-play battles.",
    image: "../../assets/img/set7.png"
  },
  {
    id:8,
    title:"Skull Dragon",
    price: 700,
    description : "Youngsters can take a deep dive into the Dragon of the East legend with this LEGO® Monkie Kid™ toy playset (80037). The set reimagines classic Dragon of the East tales as underwater adventures and features a posable dragon with 2 spring-loaded shooters, a green Fire Ring, plus a detachable toy submarine with 2 stud shooters. There are also 4 LEGO minifigures, including a new-for-June-2022 Dragon of the East character, Monkie Kid and Mr. Tang with diving accessories. Monkie Kid’s stud-shooting, Golden Staff-shaped submarine and Savage’s manta ray underwater jet adds to the battle play possibilities.",
    image: "../../assets/img/set8.png"
  },
  {
    id:9,
    title:"Toruk Makto & Tree of Souls",
    price: 800,
    description : "Help a kid aged 12+ who loves science fiction animal trends with this LEGO® Avatar Toruk Makto & Tree of Souls (75574) building toy set. The set includes Jake Sully, Neytiri, Mo’at and Tsu’Tey Na’vi minifigures, a posable Toruk figure with foil wings, Direhorse figure and a buildable Tree of Souls, plus 3 environment builds with glow-in-the-dark elements.",
    image: "../../assets/img/set9.png"
  }
]




@Injectable({
  providedIn: 'root'
})
export class ShopService {

  constructor() { }

  getProducts(): Product[] {
    return products;
  }
}
