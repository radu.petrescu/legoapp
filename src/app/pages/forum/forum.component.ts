import { Component, OnInit } from '@angular/core';
import { Post, ForumService } from './forum.service';

@Component({
  selector: 'app-forum',
  templateUrl: './forum.component.html',
  styleUrls: ['./forum.component.scss']
})


export class ForumComponent implements OnInit {

  post:Post;
  posts:Post[];
  topics:string[];
  topic:string;
  isPopupVisible:boolean;
  submitButtonOptions = {
    text: "Submit",
    useSubmitBehavior: true,
    onClick: (e:any) => {
      this.posts.push(this.post);
      this.post = new Post();
      this.isPopupVisible = false;
      
    }
  }

  constructor(service: ForumService) {
    this.topics = ["Buy/Sell","MOCS","Sets"];
    this.topic = "";
    this.isPopupVisible = false;
    this.posts = [];
    this.post = service.getPost();
  }

  selectTopic(ev:any) {
    this.topic = ev.itemData;
    console.log(ev.itemData);
    console.log("yolo");
  }

  selectionButtonClicked() {
    this.topic = "";
  }

  openPopup() {
    this.isPopupVisible = true;
  }


  ngOnInit(): void {

  }

 

  

}
