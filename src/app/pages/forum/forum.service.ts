import { Injectable } from '@angular/core';




export class Post {
  title: string = '';
  topic: string = '';
  firstName: string = '';
  lastName : string = '';
  email : string = '';
  date: Date = new Date();
  content: string = '';

  constructor() { }
}

@Injectable({
  providedIn: 'root'
})
export class ForumService {

  post:Post;

  constructor() {
    this.post = new Post();
  }

  getPost() : Post {
    return this.post;
  }

}
