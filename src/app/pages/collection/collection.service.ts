import { Injectable } from '@angular/core';


export interface LegoSet {
  id: number,
  name: string,
  collection: string,
  price : number,
  releaseDate : Date,

}

const collection: LegoSet[] = [
  {
  "id":1,
  "name": "Fire Dragon",
  "collection": "Ninjago",
  "price" : 20,
  "releaseDate" : new Date()
  },
  {
    "id":2,
    "name": "Mage Castle",
    "collection": "Kingdom",
    "price" : 30,
    "releaseDate" : new Date()
  },
  {
    "id":3,
    "name": "Water Dragon",
    "collection": "Ninjago",
    "price" : 40,
    "releaseDate" : new Date()
  }
]

@Injectable({
  providedIn: 'root'
})
export class CollectionService {

  constructor() { }


  getCollection(): LegoSet[] {
    return collection;
  }
}
