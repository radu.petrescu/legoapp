import { Component, OnInit } from '@angular/core';
import { LegoSet, CollectionService } from './collection.service';

@Component({
  selector: 'app-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.scss']
})
export class CollectionComponent implements OnInit {

  collection : LegoSet[] = [];

  constructor(service: CollectionService) {
    this.collection = service.getCollection();
  }

  selectSets(e:any) {
    console.log(e);
  }

  ngOnInit(): void {
  }

}
