import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})

export class NavigationComponent implements OnInit {


  isDrawerOpen: boolean = false;
  buttonOptions: any = {
        icon: "menu",
        onClick: () => {
            this.isDrawerOpen = !this.isDrawerOpen;
        }
  }
  navigation: any[] = [
        { id: 1, text: "Home", icon: "home", path: "home" },
        { id: 2, text: "Shop", icon: "cart", path: "shop" },
        { id: 3, text: "Forum", icon: "help", path: "forum" },
        { id: 4, text: "Collection", icon: "box", path: "collection" }
       ]

  constructor() {
   
  }

  ngOnInit(): void {
  }

}
