import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { DxButtonModule, DxDataGridModule, DxDrawerModule, DxFormModule, DxGalleryModule, DxListModule, DxPopupModule, DxToolbarModule } from 'devextreme-angular';
import { NavigationComponent } from './navigation/navigation.component';
import { ShopComponent } from './pages/shop/shop.component';
import { ForumComponent } from './pages/forum/forum.component';
import { CollectionComponent } from './pages/collection/collection.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    ShopComponent,
    ForumComponent,
    CollectionComponent, 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DxDrawerModule,
    DxToolbarModule,
    DxDataGridModule,
    DxListModule,
    DxButtonModule,
    DxPopupModule,
    DxFormModule,
    DxGalleryModule,
    
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
